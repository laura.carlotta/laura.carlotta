## <div align="center"> Fala, galeeeeeeeeera! Eu sou a Laura, mas pode me chamar de Carlota! </div>


<div align="center"> 🔭 I’m currently working at Bom Valor! 🔭 </div>
<div align="center"> 🌱 I’m currently learning Typescript! 🌱 </div>
<div align="center"> 👯 I’m looking to collaborate on Javascript! 👯 </div>
<div align="center"> 🤔 I’m looking for help with everything about Front-end, this universe is giant! 🤔 </div>
<div align="center"> 💬 Ask me about css, Maybe I can help you! 💬 </div>
<div align="center"> 📫 How to reach me: carlotta.custodio@gmail.com 📫 </div>
<div align="center"> ⚡ Fun fact: I love origamis, patins and read HQs! ⚡ </div>

##

<div align="center">
  <a href="https://gitlab.com/laura.carlotta">
  <img height="180em" src="https://gitlab-readme-stats.vercel.app/api?username=laura.carlotta&show_icons=true&theme=dark&include_all_commits=true&count_private=true"/>
  <img height="180em" src="https://github-readme-stats.vercel.app/api/top-langs/?username=lauracarlotta&layout=compact&langs_count=7&theme=dark"/>
</div>
<div align="center" style="display: inline_block"><br>
  <img align="center" alt="HTML" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original.svg">
  <img align="center" alt="CSS" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-original.svg">
  <img align="center" alt="Js" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-plain.svg">
  <img align="center" alt="Ts" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/typescript/typescript-plain.svg">
  <img align="center" alt="Vue" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/vuejs/vuejs-original.svg">
  <img align="center" alt="Python" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-original.svg">
  <img align="center" alt="Cplusplus" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/cplusplus/cplusplus-original.svg">
  <img align="center" alt="HTML" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/jquery/jquery-original.svg">
</div>
  
  ##
 
<div align="center">
  <a href="https://www.instagram.com/carlotta.front" target="_blank"><img src="https://img.shields.io/badge/-Instagram-%23E4405F?style=for-the-badge&logo=instagram&logoColor=white" target="_blank"></a>
  <a href="https://medium.com/@laura.carlotta" target="_blank"><img src="https://img.shields.io/badge/Medium-12100E?style=for-the-badge&logo=medium&logoColor=white" target="_blank"></a>
  <a href="https://discord.gg/YfYjuNyC" target="_blank"><img src="https://img.shields.io/badge/Discord-7289DA?style=for-the-badge&logo=discord&logoColor=white" target="_blank"></a>
 	<a href="https://www.twitch.tv/lauracarlottadev" target="_blank"><img src="https://img.shields.io/badge/Twitch-9146FF?style=for-the-badge&logo=twitch&logoColor=white" target="_blank"></a>
  <a href="https://github.com/lauracarlotta" target="_blank"><img src="https://img.shields.io/badge/GitHub-100000?style=for-the-badge&logo=github&logoColor=white" target="_blank"></a>
 <a href="https://open.spotify.com/user/22wimu6j4yf2ixziofbzpunta" target="_blank"><img src="https://img.shields.io/badge/Spotify-1ED760?&style=for-the-badge&logo=spotify&logoColor=white" target="_blank"></a> 
  <a href = "mailto:carlotta.custodio@gmail.com"><img src="https://img.shields.io/badge/-Gmail-%23333?style=for-the-badge&logo=gmail&logoColor=white" target="_blank"></a>
  <a href="https://www.linkedin.com/in/lauracarlotta" target="_blank"><img src="https://img.shields.io/badge/-LinkedIn-%230077B5?style=for-the-badge&logo=linkedin&logoColor=white" target="_blank"></a>
 
  ![Snake animation](https://github.com/rafaballerini/rafaballerini/blob/output/github-contribution-grid-snake.svg)
